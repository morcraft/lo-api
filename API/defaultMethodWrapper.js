const _ = require('lodash')
const swal = require('sweetalert2')
const checkTypes = require('check-types')
const Promise = require('bluebird')
const queryHandler = require('./queryHandler.js')

module.exports = function (args) {
    if (!_.isObject(args))
        throw new Error('Invalid arguments')

    if(!(args.queryHandler instanceof queryHandler))
        throw new Error('A queryHandler is expected.')

    var self = this

    self.APIComponents = {
        concepts: require('../concepts/index.js'),
        media: require('../media/index.js'),
        finishedTopics: require('../finishedTopics/index.js'),
        utils: require('../utils/index.js'),
        activities: require('../activities/index.js'),
    }

    self.makeRequest = function (_args) {
        var self = this
        if (!_.isObject(_args)) _args = {}
        return new Promise(function (resolve, reject) {

            _.forEach(['component', 'method'], function (v) {
                try {
                    checkTypes.assert.string(_args[v])
                } catch (error) {
                    return reject(new Error('Error while validating field "' + v + '" in arguments. ' + error.message))
                }
            })

            self.component = self.APIComponents[_args.component]
            try {
                checkTypes.assert.object(self.component)
            } catch (error) {
                return reject(new Error('Component "' + _args.component + '" isn\'t an object. Received ' + typeof self.component))
            }

            self.method = self.component[_args.method]

            try {
                checkTypes.assert.function(self.method)
            } catch (error) {
                return reject(new Error('Method "' + _args.method + '" in component "' + _args.component + '" isn\'t a function. Received ' + typeof self.method))
            }

            if (!_.isObject(_args.arguments)) {
                if (_args.showFeedback !== false) {
                    console.warn('Making request with no arguments.')
                }
            }

            try {
                var response = self.method(_.merge({
                    queryHandler: args.queryHandler
                }, args.requestArguments, _args))
            } catch (error) {
                reject(error)
                throw error
            }

            response
                .catch(function (error) {
                    if (_args.showUserModals !== false) {
                        swal({
                            title: 'Ocurrió un error al interactuar con el servidor',
                            type: 'error',
                            text: 'Intenta continuar con el OVA luego y verifica tu conexión a internet. Si el problema persiste, contacta al administrador.',
                            customClass: 'ova-api-wrapper-modal'
                        })
                        if (_args.showFeedback !== false) {
                            console.error(error)
                        }
                    }
                    reject(error)
                })
                .then(function (response) {
                    try {
                        var json = response.json()
                    } catch (error) {
                        if (_args.showFeedback !== false) {
                            console.error(response)
                        }
                        return reject(new Error("Invalid JSON format received. Response: ", response))
                    }
                    return json
                })
                .catch(function (error) {
                    if (_args.showFeedback !== false) {
                        console.error(error)
                    }
                    if (_args.showUserModals !== false) {
                        swal({
                            title: 'Ocurrió un error al obtener información del servidor',
                            type: 'error',
                            text: 'Contacta a un administrador. Puede haber problemas sincronizando tu interacción en el OVA.',
                            customClass: 'ova-api-wrapper-modal'
                        })
                    }
                    reject(error)
                })
                .then(function (json) {
                    if (!_.isObject(json)) {
                        if (_args.showFeedback !== false) {
                            console.error(json)
                        }
                        return reject(new Error("Response was expected in an object."))
                    }
                    if (typeof json.response === 'undefined') {
                        if (json.status === 'error' || typeof json.errorCode === 'string') {
                            if (_args.showFeedback !== false) {
                                console.warn("Response ", json)
                            }
                            return reject(new Error("Error while executing method."))
                        }
                        if (_args.showFeedback !== false) {
                            console.log('json', json)
                        }
                        return reject(new Error("No response field was received in JSON and no error code was received from API."))
                    }

                    return json.response
                })
                .catch(function (error) {
                    if (_args.showFeedback !== false) {
                        console.error(error)
                    }
                    if (_args.showUserModals !== false) {
                        swal({
                            title: 'Ocurrió un error sincronizando tu información',
                            type: 'error',
                            text: 'Contacta a un administrador.',
                            customClass: 'ova-api-wrapper-modal'
                        })
                    }
                    reject(error)
                })
                .then(function (response) {
                    resolve(response)
                })

        })
    }

}