const _ = require('lodash')
const Cookies = require('js-cookie')

module.exports = {
    localhostIndexes: ['http://localhost', 'file://', '127.0.0.1'],
    isLocalhost: function(){
        var isLocalhost = false
        _.forEach(this.localhostIndexes, function(v){
            if(location.href.indexOf(v) > -1){
                isLocalhost = true
                return true
            }
        })

        return isLocalhost
    },
    get: function(args){
        if(!_.isObject(args)) args = {}
    
        var cookieValue = null

        if(args.fakeCookie === true || this.isLocalhost()){
            if(args.showFeedback !== false)
                console.warn('Testing API with fake Moodle cookie.')

            if(_.isString(args.cookieValue))
                cookieValue = args.cookieValue
            else
                cookieValue = '5rd3ljnjt5kbaa6mt5q3rofn54' //preset Moodle Session

            if(args.showFeedback !== false)
                console.warn('Setting fake cookie value ' + cookieValue)
        }
        else{
            cookieValue = Cookies.get('MoodleSession')
            if(_.isUndefined(cookieValue))
                throw new Error('No se pudo encontrar la cookie de Moodle. Contacta al administrador del curso.')
            
            if(!_.isString(cookieValue))
                throw new Error('Tipo de dato incorrecto encontrado en la cookie de Moodle. Contacta al administrador del curso.')
        }

        return cookieValue
    }
}