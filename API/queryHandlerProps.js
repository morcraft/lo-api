const _ = require('lodash')

module.exports = function(args){
    if(!_.isObject(args))
        args = {}

    this.offline = args.offline === true
}