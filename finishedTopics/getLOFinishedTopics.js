const _ = require('lodash')
const finishedTopicsHandler = require('./finishedTopicsHandler.js')
const checkTypes = require('check-types')

module.exports = function (args) {
    checkTypes.assert.object(args) 
    return finishedTopicsHandler(_.merge({
        method: 'getLOFinishedTopics'
    }, args))
  
}