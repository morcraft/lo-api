const _ = require('lodash')
const mediaHandler = require('./mediaHandler.js')
const checkTypes = require('check-types')

module.exports = function (args) {
    checkTypes.assert.object(args)
    return mediaHandler(_.merge({
        method: 'deleteLOMedia'
    }, args))
}