const _ = require('lodash')
const conceptsHandler = require('./conceptsHandler.js')
const checkTypes = require('check-types')

module.exports = function (args) {
    checkTypes.assert.object(args)
    return conceptsHandler(_.merge({
        method: 'getLOConcepts'
    }, args))
}