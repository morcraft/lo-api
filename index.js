const _ = require('lodash')

module.exports = function(args){
    if(!_.isObject(args))
        args = {}

    const self = this

    const components = {
        concepts: require('./concepts/index.js'),
        media: require('./media/index.js'),
        finishedTopics: require('./finishedTopics/index.js'),
        utils: require('./utils/index.js'),
        course: require('./course/index.js'),
        activities: require('./activities/index.js'),
    } 

    const defaultMethodWrapper = require('./API/defaultMethodWrapper.js')

    if(args.defaultMethodWrapper === false){
        self.components = components  
    }
    else{
        const queryHandler = require('./API/queryHandler.js')
        self.queryHandler = new queryHandler(args)
        _.merge(self, new defaultMethodWrapper(_.merge({
            queryHandler: self.queryHandler,
        }, args)))
    }
}
    