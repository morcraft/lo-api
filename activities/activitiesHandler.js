const _ = require('lodash')
const queryHandler = require('../API/queryHandler.js')
const checkTypes = require('check-types')
const moodleCookie = require('../API/moodleCookie.js')

module.exports = function(args){
    args = _.isObject(args) ? args : {}
    if(!_.isObject(args.arguments))
        args.arguments = {}

    if(!(args.queryHandler instanceof queryHandler))
        throw new Error('Invalid queryHandler instance')

    this.defaultRequestArguments = {
        expectedArguments: {
            shortName: 'string',
            unit: 'number',
        },
        extraArguments: {
            sessionId: moodleCookie.get(args)
        }
    }

    this.methods = {
        getLOActivities: _.merge({
            requestType: 'GET',
        }, this.defaultRequestArguments),
        deleteLOActivities: _.merge({
            requestType: 'DELETE',
            expectedArguments: {
                activityId: 'string',
            }
        }, this.defaultRequestArguments),
        addLOActivities: _.merge({
            requestType: 'POST',
            expectedArguments: {
                activityId: 'string',
                answers: 'string'
            }
        }, this.defaultRequestArguments)
    }

    this.selectedMethod = this.methods[args.method]
    if(!_.isObject(this.selectedMethod))
        throw new Error("Method " + args.method + " isn't a part of the handler.")
    
    _.forEach(this.selectedMethod.expectedArguments, function(v, k){
        try{
            checkTypes.assert[v](args.arguments[k])
        }
        catch(err){
            throw new Error('Error while validating argument ' + k + ' with data type ' + v + '. Error:' + err.message)
        }
    })

    const queryHandlerArguments = _.merge({
        arguments: _.merge({}, args.arguments, this.selectedMethod.extraArguments)
    }, args)

    if(args.queryHandler.offline === true){
        const offline = require('./offline/index.js')
        return offline[this.selectedMethod](queryHandlerArguments)
    }


    return args.queryHandler.request(queryHandlerArguments)
   
}