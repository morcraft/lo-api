const _ = require('lodash')
const activitiesHandler = require('./activitiesHandler.js')
const checkTypes = require('check-types')

module.exports = function (args) {
    checkTypes.assert.object(args)
    return activitiesHandler(_.merge({
        method: 'getLOActivities'
    }, args))
}