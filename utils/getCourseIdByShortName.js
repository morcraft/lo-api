const _ = require('lodash')
const queryHandler = require('../API/queryHandler.js')
const checkTypes = require('check-types')

module.exports = function (args) {
    args = _.isObject(args) ? args : {}
    if(!_.isObject(args.arguments)){
        args.arguments = {}
    }

    if(!(args.queryHandler instanceof queryHandler))
        throw new Error('Invalid queryHandler instance')

    checkTypes.assert.object(args)
    checkTypes.assert.string(args.arguments.shortName)
    
    return args.queryHandler.request(_.merge({
        method: 'getUserId',
        arguments: _.merge({
            sessionId: moodleCookie.get(args)
        }, args.arguments)
    }, args))
}