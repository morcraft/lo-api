const _ = require('lodash')
const conceptsHandler = require('../concepts/conceptsHandler.js')
const mediaHandler = require('../media/mediaHandler.js')
const courseHandler = require('../course/courseHandler.js')
const finishedTopicsHandler = require('../finishedTopics/finishedTopicsHandler.js')
const teamHandler = require('../team/teamHandler.js')
const activitiesHandler = require('lo-api/activities/activitiesHandler.js')
const queryHandler = require('../API/queryHandler.js')


module.exports = function (args) {
    args = _.isObject(args) ? args : {}
    if(!_.isObject(args.arguments)){
        args.arguments = {}
    }
    
    if(!(args.queryHandler instanceof queryHandler))
        throw new Error('Invalid queryHandler instance')
        
    const queries = {
        concepts: conceptsHandler(_.merge({}, args, {
            method: 'getLOConcepts',
            returnQuery: true
        })),
        media: mediaHandler(_.merge({}, args, {
            method: 'getLOMedia',
            returnQuery: true
        })),
        finishedTopics: finishedTopicsHandler(_.merge({}, args, {
            method: 'getLOFinishedTopics',
            returnQuery: true
        })),
        courseActivities: courseHandler(_.merge({}, args, {
            method: 'getCourseActivities',
            returnQuery: true
        })),
        team: teamHandler(_.merge({}, args, {
            method: 'getLOTeam',
            returnQuery: true
        })),
        activities: activitiesHandler(_.merge({}, args, {
            method: 'getLOActivities',
            returnQuery: true
        }))
    }

    if(args.queryHandler.offline === true){
        return {
            response: queries
        }
    }

    return args.queryHandler.request({
        showFeedback: args.showFeedback,
        returnQuery: args.returnQuery,
        queries: queries
    })
}