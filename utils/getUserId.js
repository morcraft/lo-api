const _ = require('lodash')
const moodleCookie = require('../API/moodleCookie.js')
const queryHandler = require('../API/queryHandler.js')

module.exports = function (args) {
    if(!_.isObject(args))
        args = {}

    if(!(args.queryHandler instanceof queryHandler))
        throw new Error('Invalid queryHandler instance')
    
    return args.queryHandler.request(_.merge({
        method: 'getUserId',
        arguments: _.merge({
            sessionId: moodleCookie.get(args)
        }, args.arguments)
    }, args))
}