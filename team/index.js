module.exports = {
    getLOTeam: require('./getLOTeam.js'),
    addLOTeam: require('./addLOTeam.js'),
    deleteLOTeam: require('./deleteLOTeam.js')
}