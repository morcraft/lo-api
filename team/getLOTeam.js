const _ = require('lodash')
const teamHandler = require('./teamHandler.js')
const checkTypes = require('check-types')

module.exports = function (args) {
    checkTypes.assert.object(args)
    return teamHandler(_.merge({
        method: 'getLOTeam'
    }, args))
}