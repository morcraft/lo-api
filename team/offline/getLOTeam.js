module.exports = function(args){
    return {
        team: {
            "groups": [
                {
                    "group": "Autores",
                    "members": [
                        {
                            "firstName": "Nombre",
                            "lastName": "Apellido"
                        }
                    ]
                }, 
                {
                    "group": "Coordinadores de Diseño y Desarrollo de Contenidos",
                    "members": [
                        {
                            "firstName": "Nombre",
                            "lastName": "Apellido"
                        }, 
                        {
                            "firstName": "Nombre",
                            "lastName": "Apellido"
                        }
                    ]
                }
            ]
        }
    }
}